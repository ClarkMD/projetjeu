//window.onload :  permet de lancer une fonction dès l'ouverture de la fenêtre

window.onload = function()
{
    var canvasWidth = 900;
    var canvasHeight = 600;
    var blockSize = 30;
    var ctx;


    //dans delay le temps est exprimé en milliseconde donc 1000 ms = 1 s pour plus de fluidité dans le mouvement
    var delay = 100;
    var snakee;
    var applee;


    //les deux lignes suivantes permettent de diviser lle canvas en blocks
    var widthInBlocks = canvasWidth/blockSize; //30 blocks numérotés de 0 à 29
    var heightInBlocks = canvasHeight/blockSize; //20 blocks numérotés de 0 à 19
    var score; // le score du jeu     
    var Timeout;


    init();


// ------------------------------- Ici on créé la structure du jeu c'est ce qui s'affichera lors de l'ouverture de la page ---------------------------------------------------------------------
   
    //la fonction init : permet d'initialiser
    function init()
    {
        //canvas est un élément html qui permet de "dessiner" sur une page html
        var canvas = document.createElement('canvas');
        canvas.width = canvasWidth;
        canvas.height = canvasHeight ;
        canvas.style.border = "15px solid black";
        canvas.style.margin = "50px auto";
        canvas.style.display = "block";
        canvas.style.backgroundColor = "#ddd";
       
        //document.body permet de selectionner la partie body dans l'ensemble du html.  -- appendChild(Canvas) permet d'accrocher un tag à la partie body
        document.body.appendChild(canvas);
        ctx = canvas.getContext('2d');
        snakee = new Snake([[6,4],[5,4],[4,4],[3,4],[2,4]], "right");
        applee = new Apple([10,10]);

        //cette ligne active la fonction refrechCanvas à la fin de la fonction init
        score = 0;
        refreshCanvas();
    }
   
    function refreshCanvas()
{
    snakee.advance();
    if(snakee.checkCollision())
    {
        gameOver();

    }
    else
    {
        if(snakee.isEatingApple(applee))
        {
           score ++;
           snakee.ateApple = true;
            do
           {
            applee.setNewPosition();
           }
           while(applee.isOnSnake(snakee))
       
        }

        //.clearRect est une fonction qui efface le contenu du canvas, on lui donne 4 valeurs : les deux premières definissent le point de depart de l'effacement et les deux derniere les points d'arriver. Ici on part du point 0,0 et on le fait sur toute la surface.
        ctx.clearRect(0,0, canvasWidth, canvasHeight);
        drawScore();
        snakee.draw();
        applee.draw();
       
        //setTimeout permet de d'éxécuter une fonction voulu à chaque fois qu'un certain delai est passé.  La fonction setTimeout execute la fonction refreshCanvas grace à delay, soit 100 ms
        timeout = setTimeout(refreshCanvas,delay);
    }
}
   

       function gameOver()
       {
           ctx.save();
           ctx.font = "bold 70px sans-serif";
           ctx.fillStyle = "black";
           ctx.textAlign = "center";
           ctx.textBaseLine = "middle";
           ctx.strokeStyle = "white";
           ctx.lineWidth = 5;
           var centreX = canvasWidth/ 2;
           var centreY = canvasHeight/ 2;
           ctx.strokeText("Game Over", centreX, centreY - 180);
           ctx.fillText("Game Over", centreX, centreY - 180);
           ctx.font = "bold 30px sans-serif";
           ctx.strokeText("Appuyer sur la touche Espace pour rejouer", centreX, centreY - 120);
           ctx.fillText("Appuyer sur la touche Espace pour rejouer", centreX, centreY - 120);
           ctx.restore();

       }

       function restart()
       {
        snakee = new Snake([[6,4],[5,4],[4,4],[3,4],[2,4]], "right");
        applee = new Apple([10,10]);

        // Active la fonction refreshCanvas à la fin de la fonction init
        score = 0;
        clearTimeout(timeout);
        refreshCanvas();
       }


       function drawScore()
       {
        ctx.save();
        ctx.font = "bold 80px sans-serif";
        ctx.fillStyle = "black";
        ctx.textAlign = "center";
        ctx.textBaseLine = "middle";
        var centreX = canvasWidth/ 2;
        var centreY = canvasHeight/ 2;
        ctx.fillText(score.toString(),centreX,centreY);
        ctx.restore();
       }

       //Dessin du serpent
       function drawBlock(ctx, position)
    {
        var x = position[0] * blockSize;
        var y = position[1] * blockSize;
       
        //.fillRect : fonction à quatre arguments : le x (distance horizontale), le y (distance verticale) puis la dimention x de l'objet à créer (sa largeur), et enfin sa dimention y (sa hauteur).
        ctx.fillRect(x ,y , blockSize, blockSize);
    }
   
    function Snake(body, direction){
        this.body = body;
        this.direction = direction;
        this.ateApple = false;
        this.draw = function()
        {
                //on sauvegarde les caractéristiques du serpent avant l'execution de cette fonction.
                ctx.save();
                ctx.fillStyle = "#38610B";
                for(var i = 0; i < this.body.length; i++)
                {
                    drawBlock(ctx, this.body[i]);
                }
                ctx.restore();
        };
        this.advance = function()
        {
                var nextPosition = this.body[0].slice()

                //.slice créé une copie de l'élément
                switch(this.direction)
                {
                        case "left":
                            nextPosition[0] -= 1;
                            break;
                        case "right":
                            nextPosition[0] += 1;
                            break;
                        case "down":
                            nextPosition[1] += 1;
                            break;
                        case "up":
                            nextPosition[1] -= 1;
                            break;
                        default:
                            //fonction qui permet de signaler une erreur par un message.
                            throw("Invalid Direction");
                    }
                //.unshift permet de rajouter un élément en première position dans un aray.
                this.body.unshift(nextPosition)
                //.pop permet de supprimer le dernier élément dans un aray.
                if(!this.ateApple)
                   this.body.pop();
                else
                   this.ateApple = false;


            };
       
//Ici on etablit les restrictions de deplacement du serpent. Si il se deplace de maniere horizontale ou verticale il ne peut pas faire un demi tour sur lui même.
       
            this.setDirection = function(newDirection)
            {
                var allowedDirections;
                switch(this.direction)
                {
                        case "left":
                        case "right":
                            allowedDirections = ["up", "down"];
                            break;
                        case "down":
                        case "up":
                            allowedDirections = ["left", "right"]; 
                            break;
                        default:
                            throw("Invalid Direction");
                }
                if(allowedDirections.indexOf(newDirection) > -1)
                {
                     this.direction = newDirection;
                }
            };
        // On verifie si le serpent entre en collision avec un obstace
            this.checkCollision = function()
            {
                var wallCollision = false;
                var snakeCollision = false;
                var head = this.body[0];
                var rest = this.body.slice(1);
                var snakeX = head[0];
                var snakeY = head[1];
                var minX = 0;
                var minY = 0;
                var maxX = widthInBlocks - 1;
                var maxY = heightInBlocks - 1;
                var isNotBetweenHorizontalWalls = snakeX < minX || snakeX > maxX;
                var isNotBetweenVerticalWalls = snakeY < minY || snakeY > maxY;
               
        // On verifie si le serpent se prend un mur       
                if(isNotBetweenHorizontalWalls || isNotBetweenVerticalWalls)
                {
                    wallCollision = true;
                }
               
        // On verifie si le serpent se prend son propre corps
                for(var i = 0; i < rest.length; i++)
                {
                    if(snakeX === rest[i][0] && snakeY === rest[i][1] )
                    {
                        snakeCollision = true;
                    }
                }
                return wallCollision || snakeCollision;
            };
            this.isEatingApple = function(appleToEat)
            {
               var head = this.body[0];
               if (head[0]=== appleToEat.position[0] && head[1] === appleToEat.position[1])
                   return true;
                else
                   return false;

            };
    }
   
//On créé les pommes pour le serpent
   
    function Apple(position)
    {
        this.position = position;
        this.draw = function()
        {
            //ctx.save sauvegarde la configuration avant de dessiner un nouvel élément.
            ctx.save();
            //ctx.fillStyle change les caractéristique pour dessiner un élément.
            ctx.fillStyle = "#B40404";
            ctx.beginPath();
            var radius = blockSize/2;
            var x = this.position[0]*blockSize + radius;
            var y = this.position[1]*blockSize + radius;
            //code pour dessiner un rond avec X et Y qui determinent les rayons sur ces deux axes.
            ctx.arc(x,y, radius, 0, Math.PI*2, true);
            ctx.fill();
            //ctx.restore remet la configuration antérieure une fois que l'élément précédent est dessiné.
            ctx.restore();
        };
        this.setNewPosition = function()
        {
           
            var newX = Math.round(Math.random() * (widthInBlocks - 1));
            var newY = Math.round(Math.random() * (heightInBlocks - 1));

          this.position = [newX, newY];
        };
        this.isOnSnake = function(snakeToCheck)
        {
           var isOnSnake = false;

           for (var i = 0 ; i < snakeToCheck.body.length; i++ )
           {
               if(this.position[0] === snakeToCheck.body[i][0] && this.position[1] === snakeToCheck.body[i][1] )
               {
                   isOnSnake = true;
               }
          
          
            }
            return isOnSnake;
      
        };
    }
   
//Ici on determine comment diriger le serpent
   
    //document.onkeydown permet de lancer une fonction quand l'utilisateur appuit sur une touche de son clavier
    document.onkeydown = function handleKeyDown(e)
    {
        var key = e.keyCode;
        var newDirection;
        switch(key)
        {
            case 37:
                newDirection = "left";
                break;
            case 38:
                newDirection = "up";
                break;
            case 39:
                newDirection =  "right";
                break;
            case 40:
                newDirection = "down";
                break;
            case 32:
               restart();
               return;
            default:
                return;
        }
        snakee.setDirection(newDirection);
    }
}


